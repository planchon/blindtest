const express = require("express");
const fetch = require("node-fetch");
const qs = require("querystring");

const app = express();
const cors = require("cors");

const TAILLE_PLAYLIST = 5;

const PROD = false
const FRONT_URI = PROD ? "http://music.planchon.io" : "http://localhost:8080"
const SOCKET_URI = PROD ? "http://music.planchon.io:3000" : "http://localhost:3000"

app.use(cors());

var io = require("socket.io").listen(app.listen(3000));

var user_database = {};

function shuffle (array) {

	var currentIndex = array.length;
	var temporaryValue, randomIndex;

	// While there remain elements to shuffle...
	while (0 !== currentIndex) {
		// Pick a remaining element...
		randomIndex = Math.floor(Math.random() * currentIndex);
		currentIndex -= 1;

		// And swap it with the current element.
		temporaryValue = array[currentIndex];
		array[currentIndex] = array[randomIndex];
		array[randomIndex] = temporaryValue;
	}

	return array;

};

function generateUniqueRandomList(data) {
    var arr = [];
    
    while (arr.length < TAILLE_PLAYLIST) {
        var r = Math.floor(Math.random() * data.length);
        if ((arr.indexOf(r) === -1) && (data[r].track.preview_url !== null)) arr.push(r);
    }

    data = data.filter((item, index) => {
        return (arr.indexOf(index) !== -1)
    })

    return data;
}

function changeScore(uuid, score, room) {
    for (var i = 0; i < user_database[room].user.length; i++) {
        if (user_database[room].user[i].uuid == uuid) {
            user_database[room].user[i].score += score
        }
    }
}

function changeState(uuid, state, room) {
    for (var i = 0; i < user_database[room].user.length; i++) {
        if (user_database[room].user[i].uuid == uuid) {
            user_database[room].user[i].ready = state
        }
    }
}

function resetState(room, state) {
    for (var i = 0; i < user_database[room].user.length; i++) {
        user_database[room].user[i].ready = state
    }
}

app.get("/auth/begin", (req, res) => {
    let code = req.query.code;

    let spotify_url = "https://accounts.spotify.com/api/token";

    const data = {
        grant_type: "authorization_code",
        code: code,
        redirect_uri: FRONT_URI + "/create"
    };

    console.log(data.redirect_uri)

    fetch(spotify_url, {
        method: "POST",
        body: qs.stringify(data),
        headers: {
            Authorization:
                "Basic ODg3YjM1ZmMyODkyNGMxNGI5MWU1NzlkNjc1ZWI5NzA6YjcxYmMyMGNlY2UxNDExMjkzY2YyNDkzYTMxODBkMmE=",
            "Content-Type": "application/x-www-form-urlencoded"
        }
    })
        .then(resp => resp.json())
        .then(resp => {
            res.send(resp);
        });
});

app.get("/user/playlist", (req, res) => {
    var access = req.query.access;

    fetch("https://api.spotify.com/v1/me/playlists", {
        headers: { Authorization: "Bearer " + access }
    })
        .then(resp => resp.json())
        .then(resp => res.send(resp));
});

app.get("/room/get_id", (req, res) => {
    let number = Object.keys(user_database);
    let hypo_room = Math.ceil(1000 + Math.random() * 9000);
    while (hypo_room in number) {
        hypo_room = Math.ceil(1000 + Math.random() * 9000);
    }

    res.send({ id: hypo_room });
});

app.get("/room/:id/playlist", (req, res) => {
    let id = req.params.id
    res.send(user_database[id].data)
})

app.get("/user/playlist/track", (req, res) => {
    var access = req.query.access;
    var id = req.query.id;

    fetch(`https://api.spotify.com/v1/playlists/${id}/tracks`, {
        headers: { Authorization: "Bearer " + access }
    })
        .then(resp => resp.json())
        .then(resp => res.send(resp));
});

io.on("connection", socket => {

    // quand un user se connecte à l'appli
    socket.on("new user", data => {
        socket.join(data.room);

        if (!user_database[data.room]) {
            user_database[data.room] = {user: [], data: [], ready: 0, current: 0}
            user_database[data.room].user = [{name: data.pseudo, uuid: data.uuid, score: 0, ready: false}]
        } else {
            let db = user_database[data.room].user;
            db.push({name: data.pseudo, uuid: data.uuid, score: 0, ready: false});
            user_database[data.room].user = db;
        }

        let users = user_database[data.room].user
        console.log(users)

        io.sockets
            .in(data.room)
            .emit("change user list", users);
    });

    socket.on("admin wants start", data => {
        console.log("admin wants start");
        io.sockets.in(data.room).emit("start", {});
    });

    // quand quelquun creer une nouvelle room
    // on ajoute a la bdd la nouvelle room
    // et on genere la playlist
    socket.on("admin new room", data => {

        // TODO prendre toutes les musiques de la playlist (si il y en a plus que 100)
        fetch(`https://api.spotify.com/v1/playlists/${data.playlist}/tracks`, {
            headers: { Authorization: "Bearer " + data.access }
        })
            .then(resp => resp.json())
            .then(resp => {
                let songData = generateUniqueRandomList(resp.items);
                let finalData = songData.map((e) => {
                    return fetch(`https://api.spotify.com/v1/recommendations?seed_tracks=${e.track.id}`, {
                        headers: { Authorization: "Bearer " + data.access }
                    })
                    .then(res => res.json())
                    .then(res => {
                        let rawData = res.tracks
                        rawData.sort((a, b) => {
                            return a.popularity - b.popularity
                        })
                        rawData.reverse()
                        let finalData = shuffle([{name: e.track.name, id: e.track.id},
                                                 {name: rawData[0].name, id: rawData[0].id}, 
                                                 {name: rawData[1].name, id: rawData[1].id}, 
                                                 {name: rawData[2].name, id: rawData[2].id}])
                        return {
                            url: e.track.preview_url,
                            res: finalData,
                            verif: e.track.id,
                            response: 0
                        }
                    })
                })
                Promise.all(finalData).then((values) => {
                    user_database[data.data].data = values
                    socket.emit("admin can start", values);
                })
            });
    });

    // il faut que tous les joueurs soient ready pour lancer la partie 
    socket.on("ready", data => {
        user_database[data.room].ready = user_database[data.room].ready + 1

        if (user_database[data.room].current > 0) {
            changeState(data.uuid, true, data.room)
            console.log(user_database[data.room].user)
            io.sockets.in(data.room).emit("show score", user_database[data.room].user)
        }

        if (user_database[data.room].ready == user_database[data.room].user.length) {
            console.log("la game est prete on y va")

            if (user_database[data.room].current < TAILLE_PLAYLIST) {
                setTimeout(() => {
                    io.sockets.in(data.room).emit("play song", {index: user_database[data.room].current})
                    user_database[data.room].current++
                    user_database[data.room].ready = 0
                    resetState(data.room, false)
                }, 1000)
            } else {
                io.sockets.in(data.room).emit("the end", user_database[data.room].user)
            }

        }
    })

    socket.on("response", data => {
        let current = user_database[data.room].current - 1

        if (data.answer == user_database[data.room].data[current].verif) {
            if (data.time < 5000) {
                changeScore(data.uuid, data.time, data.room)
            } else {
                changeScore(data.uuid, 5000, data.room)
            }
        } else {
            changeScore(data.uuid, 10000, data.room)
        }
        user_database[data.room].data[current].response++

        if (user_database[data.room].data[current].response == user_database[data.room].user.length) {
            io.sockets.in(data.room).emit("show answer", {})
            setTimeout(() => {
                io.sockets.in(data.room).emit("show score", user_database[data.room].user)
            }, 3000)
        }
    })
});