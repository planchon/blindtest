from flask import Flask
app = Flask(__name__, static_url_path="/assets", 
static_folder="./assets")

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    print(path)
    if "main.js" in path:
        return app.send_static_file("main.js")
    else:
        return app.send_static_file("index.html")

if __name__ == '__main__':
    app.run(port=8080)