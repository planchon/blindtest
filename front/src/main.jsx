import React from 'react'
import ReactDom from 'react-dom'
import App from './components/app'
import {AppContainer} from 'react-hot-loader'
import './main.scss'

import "shards-ui/dist/css/shards.min.css"

const renderApp = () => {
  ReactDom.render(
    <AppContainer><App className="app"/></AppContainer>,
    document.getElementById('app')
  )
}

if (module.hot) {
  module.hot.accept('./components/app', renderApp)
}

renderApp() 