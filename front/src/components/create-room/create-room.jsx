import React from "react";
import queryString from "query-string";
import Cookies from "universal-cookie";

import Lottie from "react-lottie";
import { data } from "./data";
import { FRONT_URI, SOCKET_URI } from "../../var";

import {
    ListGroup,
    ListGroupItem,
    ListGroupItemText,
    Button,
    ButtonGroup
} from "shards-react";

import "./create-room.scss";
import { rejects } from "assert";
import { domainToASCII } from "url";

const cookies = new Cookies();

export default class CreateRoom extends React.Component {
    constructor(props) {
        super(props);

        this.code = queryString.parse(this.props.location.search).code;

        this.goToRoom = this.goToRoom.bind(this)

        this.state = {
            isLoaded: false,
            hasPlaylistChoosen: false,
            playlistId: "",
            playlistData: [],
            playlistHasLoaded: false,
            room: 0
        };
    }

    goToRoom() {
        cookies.set("isadmin", 1);
        cookies.set("playlist_id", this.state.playlistId)
        
        if (this.state.room) {
            window.location = "/room/" + this.state.room
        }
    }

    renderPlaylistMusic() {
        if (this.state.playlistHasLoaded) {
            return (
                <div>
                    <div className="buttons">
                        <Button onClick={() => {this.setState({hasPlaylistChoosen: false})}}>Back</Button>
                        <Button onClick={this.goToRoom}>Continue</Button>
                    </div>
                    <ListGroup className="liste">
                        {this.state.playlistData.map(item => {
                            return (
                                <ListGroupItem>
                                    {item["track"]["name"]}
                                </ListGroupItem>
                            );
                        })}
                    </ListGroup>
                </div>
            );
        } else {
            let access = cookies.get("access");
            fetch(
                SOCKET_URI + `/user/playlist/track?id=${this.state.playlistId}&access=${access}`
            )
                .then(res => {
                    return res.json();
                })
                .then(res => {
                    console.log(res);
                    let data = res.items;
                    this.setState({
                        playlistData: data,
                        playlistHasLoaded: true
                    });
                });
        }
    }

    renderPlaylist() {
        if (this.state.playlistLoaded) {
            console.log(this.state.playlist);
            return (
                <ListGroup className="liste">
                    {this.state.playlist.map(item => {
                        return (
                            <ListGroupItem
                                action
                                onClick={() => {
                                    this.setState({
                                        playlistId: item.id,
                                        hasPlaylistChoosen: true
                                    });
                                }}
                            >
                                {item.name}
                            </ListGroupItem>
                        );
                    })}
                </ListGroup>
            );
        }
    }

    componentDidMount() {
        fetch(SOCKET_URI + "/auth/begin?code=" + this.code)
            .then(res => res.json())
            .then(res => {
                if (!res["access_token"]) {
                    console.log("ERREUR");
                    window.location = "/";
                } else {
                    cookies.set("access", res["access_token"], { path: "/" });
                    cookies.set("refresh", res["refresh_token"], { path: "/" });
                    this.setState({ isLoaded: true });
                    this.getUserPlaylist();
                }
            });

        fetch(SOCKET_URI + "/room/get_id").then(
            res => res.json()
        ).then(
            res => this.setState({room: res["id"]})
        )
    }

    getUserPlaylist() {
        if (this.state.isLoaded) {
            let access = cookies.get("access");
            fetch(SOCKET_URI + "/user/playlist?access=" + access)
                .then(res => res.json())
                .then(res =>
                    this.setState({ playlist: res.items, playlistLoaded: true })
                );
        }
    }

    render() {
        const defaultOptions = {
            loop: true,
            autoplay: true,
            animationData: data,
            rendererSettings: {
                preserveAspectRatio: "xMidYMid slice"
            }
        };

        return (
            <div>
                {/* <Lottie options={defaultOptions} height={400} width={400} /> */}
                <div className="container">
                    <div className="title">
                        <h5>Choose the playlist we will use!</h5>
                        {!this.state.hasPlaylistChoosen
                            ? this.renderPlaylist()
                            : this.renderPlaylistMusic()}
                    </div>
                </div>
            </div>
        );
    }
}