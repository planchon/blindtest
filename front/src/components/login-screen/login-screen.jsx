import React from "react";

import Lottie from "react-lottie";
import { data } from "./data";
import { FRONT_URI, SOCKET_URI } from "../../var";

import Cookies from "universal-cookie"

import { Button } from "shards-react";
import { FormInput } from "shards-react";
import { Card, CardBody } from "shards-react";

import PinInput from "react-pin-input";

import "./login-screen.scss";

const cookies = new Cookies();

export default class LoginScreen extends React.Component {
    constructor(props) {
        super(props);

        this.spotifyLink = "https://accounts.spotify.com/authorize?client_id=887b35fc28924c14b91e579d675eb970&response_type=code&redirect_uri=" + FRONT_URI + "/create&scope=streaming%20playlist-read-private%20playlist-read-collaborative%20user-modify-playback-state%20user-read-private"

        cookies.set("isadmin", 0);

        this.state = {
            joinRoomButton: false,
            roomCode: "0"
        };
    }

    joinRoom() {
        if (this.state.joinRoomButton) {
            return (
                <div className="cards">
                    <Card className="card">
                        <CardBody>
                            <PinInput
                                length={4}
                                initialValue=""
                                onChange={(value, index) => {}}
                                type="numeric"
                                style={{ padding: "10px" }}
                                inputStyle={{ borderColor: "black" }}
                                inputFocusStyle={{ borderColor: "blue" }}
                                onComplete={(value, index) => { this.setState({roomCode: value})}}
                            />
                            <Button
                                disabled={!(this.state.roomCode.length == 4)}
                                className="button"
                                style={{ "margin-top": "10px" }}
                                outline
                                pill
                                theme="light"
                                onClick={() =>
                                    {window.location = "/room/" + this.state.roomCode;}
                                }
                            >
                                Join the room
                            </Button>
                        </CardBody>
                    </Card>
                </div>
            );
        } else {
            return (
                <Button
                    className="button"
                    style={{ "margin-top": "10px" }}
                    outline
                    pill
                    theme="light"
                    onClick={() => this.setState({ joinRoomButton: true })}
                >
                    Join a room
                </Button>
            );
        }
    }

    render() {
        const defaultOptions = {
            loop: true,
            autoplay: true,
            animationData: data,
            rendererSettings: {
                preserveAspectRatio: "xMidYMid slice"
            }
        };

        return (
            <div>
                <div className="logo">
                    {/* <Lottie options={defaultOptions} height={400} width={400} /> */}
                </div>
                <div className="buttons">
                    <a href={this.spotifyLink}>
                        <Button className="button" outline pill theme="success">
                            Create a room
                        </Button>
                    </a>
                    {this.joinRoom()}
                </div>
            </div>
        );
    }
}
