import React from "react";

import LoginScreen from "./login-screen/login-screen";
import CreateRoom from "./create-room/create-room"
import Room from "./room/room"

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

export default class App extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Router>
                <Switch>
                    <Route exact path="/" component={LoginScreen} />
                    <Route exact path="/room/:id" component={Room} />
                    <Route exact path="/create" component={CreateRoom} />
                </Switch>
            </Router>
        );
    }
}
