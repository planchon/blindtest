import React from "react"
import Cookies from "universal-cookie"

import Waiting from "../waiting/waiting"
import socketIOClient from "socket.io-client";
import { FRONT_URI, SOCKET_URI } from "../../var";

import Game from "../game/game"

const cookies = new Cookies();
const socket = socketIOClient(SOCKET_URI);

export default class Room extends React.Component {
    constructor(props) {
        super(props)

        let isAdmin = cookies.get("isadmin")

        this.state = {
            isAdmin: isAdmin,
            isWaiting: true,
            players: []
        }
    }

    componentDidMount() {
        if (this.state.isAdmin) {
            let play = cookies.get("playlist_id")
            this.setState({
                playlistId: play
            })
        }

        socket.on("start", () => {
            this.setState({ isWaiting: false })
        })
    }

    render () {
        if (this.state.isWaiting) {
            return <Waiting socket={socket} id={this.props.match.params.id}/>
        } else {
            return <Game id={this.props.match.params.id} socket={socket} />
        }
    }
}