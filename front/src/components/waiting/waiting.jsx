import React from "react";

import Lottie from "react-lottie";

import Cookies from "universal-cookie";

import socketIOClient from "socket.io-client";

import {
    ListGroup,
    ListGroupItem,
    FormInput,
    Button
} from "shards-react";

import { data } from "./loading";
import { light } from "./light";
import { FRONT_URI, SOCKET_URI } from "../../var";

import "./waiting.scss";
import { isThisSecond } from "date-fns/esm";

const cookies = new Cookies();

export default class Waiting extends React.Component {
    constructor(props) {
        super(props);

        let test = cookies.get("isadmin") == "1";

        this.adminStart = this.adminStart.bind(this)

        this.socket = this.props.socket

        this.state = {
            hasPseudo: false,
            roomId: parseInt(this.props.id),
            isAdmin: test,
            pseudo: "",
            owner: "the owner",
            players: [],
            adminCanStart: false
        };
    }

    componentDidMount() {
        // on rentre dans la room des sockets
        this.socket.on("change user list", data =>
            this.setState({ players: data })
        );

        if (this.state.isAdmin) {
            this.socket.on("admin can start", data => {
                this.setState({
                    adminCanStart: true
                })
            })
        }
    }

    renderPlayers() {
        console.log(this.state.players)
        return (
            <ListGroup className="liste">
                {this.state.players.map(player => {
                    return <ListGroupItem>{player.name}</ListGroupItem>;
                })}
            </ListGroup>
        );
    }

    adminStart() {
        this.socket.emit("admin wants start", { room: this.state.roomId })
    }

    waitingScreen() {
        const waitingOptions = {
            loop: true,
            autoplay: true,
            animationData: data,
            rendererSettings: {
                preserveAspectRatio: "xMidYMid slice"
            }
        };

        return (
            <div>
                {/* <Lottie options={waitingOptions} height={400} width={400} /> */}
                <div className="container">
                    <div className="title">
                        {this.state.isAdmin ? (
                            <h4>Start when you want</h4>
                        ) : (
                                <h4>Waiting for {this.state.owner} to start...</h4>
                            )}
                        {this.state.isAdmin ? (
                            <Button disabled={!this.state.adminCanStart} style={{ width: "100%" }} pill outline onClick={this.adminStart}>
                                Start
                            </Button>
                        ) : (
                                <div></div>
                            )}
                    </div>
                    <hr />
                    <div className="player">
                        <h5>Players :</h5>
                        {this.renderPlayers()}
                    </div>
                </div>
            </div>
        );
    }

    uuidv4() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }

    pseudoWasChoosen(pseudo) {
        this.setState({
            hasPseudo: true
        });

        let uuid = this.uuidv4() 

        this.socket.emit("new user", {
            pseudo: this.state.pseudo,
            uuid: uuid,
            room: this.state.roomId
        });

        cookies.set("uuid", uuid)

        if (this.state.isAdmin) {
            this.socket.emit("admin new room", {
                data: this.state.roomId,
                playlist: cookies.get("playlist_id"),
                access: cookies.get("access")
            })
        }
    }

    render() {
        const lightOption = {
            loop: true,
            autoplay: true,
            animationData: light,
            rendererSettings: {
                preserveAspectRatio: "xMidYMid slice"
            }
        };

        if (this.state.hasPseudo) {
            return this.waitingScreen();
        } else {
            var isTextValid = this.state.pseudo.length >= 4;
            return (
                <div>
                    {/* <Lottie options={lightOption} height={400} width={500} /> */}
                    <div className="container">
                        <div className="title">
                            <h4>Choose a pseudo !</h4>
                            <FormInput
                                placeholder="my name!"
                                invalid={!isTextValid}
                                valid={isTextValid}
                                onChange={e =>
                                    this.setState({ pseudo: e.target.value })
                                }
                            />
                            <Button
                                className="comp"
                                outline
                                pill
                                theme="secondary"
                                onClick={() => {
                                    if (isTextValid) {
                                        this.pseudoWasChoosen();
                                    }
                                }}
                            >
                                Go with this one
                            </Button>
                        </div>
                    </div>
                </div>
            );
        }
    }
}
