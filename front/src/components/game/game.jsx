import React from "react";

import Lottie from "react-lottie";
import Cookies from "universal-cookie"

import MusicPlayer from "../music/music.js";
import { data } from "./guitar";
import { FRONT_URI, SOCKET_URI } from "../../var";


import {
    Button, 
    ListGroup,
    ListGroupItem,
} from "shards-react";

const cookies = new Cookies();

export default class Game extends React.Component {
    constructor(props) {
        super(props);

        this.id = this.props.id;
        this.socket = this.props.socket;

        this.thePlayerIsReady = this.thePlayerIsReady.bind(this)
        this.uuid = cookies.get("uuid")

        this.state = {
            currentSong: 0,
            audio: null,
            isPlaying: false,
            dataHasLoaded: false,
            playlist: [{
                url: "#",
                res: [{name: "song1", id: 1}, {name: "song2", id: 2}, {name: "song3", id: 3}, {name: "song4", id: 4}],
                verif: 1
            }],
            players: [{ name: "user1", score: 100, ready: false }, { name: "user2", score: 256, ready: false }, { name: "user3", score: 844, ready : true }, { name: "user4", score: 865, ready: false }],
            showQuestion: false,
            showResponse: false,
            showAnswer: false,
            showScore: false,
            renderScoreClicked: false,
            showEnd: false
        };
    }

    // etats du composant (je sais les noms sont pas oufouf)
    // ------------
    // showQuestion => lance la musique et propose au user de clicker
    // showResponse => highlight en bleu la reponce de l'user
    // showAnswer   => montre la bonne reponse
    // showScore    => montre les scores 
    // isPlaying    => quand on ne montre pas les scores
    componentDidMount() {
        this.socket.on("play song", index => {
            console.log("il faut jouer une musique", index)
            this.setState({
                currentSong: index.index,
                timeBegin: performance.now()
            })
            this.isPlayingSong()
        });

        this.socket.on("show score", data => {
            this.setState({
                players: data
            })
            this.isShowingScore()
        })
        
        this.socket.on("show answer", data => {
            this.isShowingAnswer()
        })

        this.socket.on("the end", data => {
            this.isShowingEnd()
        })

        fetch(SOCKET_URI + `/room/${this.id}/playlist`)
            .then(res => res.json())
            .then(res => {

                this.setState({
                    dataHasLoaded: true,
                    audio: new MusicPlayer(res, this.thePlayerIsReady),
                    playlist: res
                });
            });
    }

    isPlayingSong() {
        this.setState({
            showResponse: false,
            showQuestion: true,
            showAnswer: false,
            showScore: false,
            isPlaying: true,
            renderScoreClicked: false
        })
        this.state.audio.play(this.state.currentSong)
    }

    isShowingResponse() {
        this.state.audio.pause()
        this.setState({
            showResponse: true,
            showQuestion: false,
            showAnswer: false,
            showScore: false,
            isPlaying: true,
        })
    }

    isShowingAnswer() {
        this.setState({
            showResponse: false,
            showQuestion: false,
            showAnswer: true,
            showScore: false,
            isPlaying: true,
        })
    }

    isShowingScore() {
        this.setState({
            showResponse: false,
            showQuestion: false,
            showAnswer: false,
            showScore: true,
            isPlaying: false,
        })
    }

    isShowingEnd() {
        this.setState({
            showResponse: false,
            showQuestion: false,
            showAnswer: false,
            showScore: false,
            isPlaying: false,
            showEnd: true
        })
    }

    thePlayerIsReady() {
        this.setState({renderScoreClicked: true})
        this.socket.emit("ready", {room: this.id, uuid: this.uuid});
    }

    optionChoosen(id) {
        this.setState({
            answerNumber: id
        })
        this.isShowingResponse()
        this.socket.emit("response", {answer: this.state.playlist[this.state.currentSong].res[id].id, uuid: this.uuid, room: this.id, time: performance.now() - this.state.timeBegin})
    }

    // --------------
    // RENDER SECTION
    // --------------

    // fais le rendu des scores
    renderScore() {
        return (
            <div>
                <h4>Scoreboard : </h4>
                <hr />
                <Button style={{ width: "100%" }} active={this.state.renderScoreClicked} pill outline onClick={this.thePlayerIsReady}>ready</Button>
                <hr />
                <ListGroup className="liste">
                    {this.state.players.map((player, i) => {
                        return <ListGroupItem theme={!player.ready ? "warning" : "success"}>{i + 1} : {player.name} - {player.score}</ListGroupItem>;
                    })}
                </ListGroup>
            </div>
        )
    }

    // fais le rendu de la bonne reponce
    renderAnswer(reponse) {
        return (<div>
            {reponse.map((e, i) => {
                if (this.state.answerNumber == i || this.state.playlist[this.state.currentSong].verif == e.id) {
                    return (
                        <Button style={{ width: "100%", marginTop: "10px" }} active pill outline theme={(this.state.playlist[this.state.currentSong].verif == e.id) ? "success" : "danger"}>
                            {e.name}
                        </Button>
                    );
                } else {
                    return (
                        <Button style={{ width: "100%", marginTop: "10px" }} disabled pill outline theme="light">
                            {e.name}
                        </Button>
                    );
                }
            })}
        </div>)
    }

    // fais le rendu de la question
    renderQuestion(reponse) {
        return (<div>
            {reponse.map((e, i) => {
                return (
                    <Button onClick={() => this.optionChoosen(i)} style={{ width: "100%", marginTop: "10px" }} pill outline>
                        {e.name}
                    </Button>
                )
            })}
        </div>)
    }

    // fais le rendu de la reponce de l'user (celle en bleu)
    renderResponse(reponse) {
        return (
            <div>
                {reponse.map((e, i) => {
                    if (this.state.answerNumber == i) {
                        return (
                            <Button style={{ width: "100%", marginTop: "10px" }} active pill outline>
                                {e.name}
                            </Button>
                        );
                    } else {
                        return (
                            <Button style={{ width: "100%", marginTop: "10px" }} disabled pill outline theme="light">
                                {e.name}
                            </Button>
                        );
                    }
                })}
            </div>
        )
    }

    // fais le rendu du jeu (s'occupe de tout sauf du score)
    renderGame() {  
        if (this.state.dataHasLoaded) {
            let song = this.state.playlist[this.state.currentSong].url;
            let reponse = this.state.playlist[this.state.currentSong].res;

            return (
                <div>
                    <h4>Which song is playling ?</h4>
                    {this.state.showQuestion ? this.renderQuestion(reponse) : <div></div>}
                    {this.state.showResponse ? this.renderResponse(reponse) : <div></div>}
                    {this.state.showAnswer ? this.renderAnswer(reponse) : <div></div>}
                </div>
            );
        } else {
            return <div>loading</div>;
        }
    }

    renderEnd() {
        return (
            <div>
                <h4>Final Scoreboard : </h4>
                <hr />
                <ListGroup className="liste">
                    {this.state.players.map((player, i) => {
                        return <ListGroupItem theme={!player.ready ? "warning" : "success"}>{i + 1} : {player.name} - {player.score}</ListGroupItem>;
                    })}
                </ListGroup>
                <hr />
                <p>thank you for playing, if you found bug(s) contact paul@planchon.io</p>
            </div>
        )
    }

    renderNotPlaying() {
        return (
            <div>
                {this.state.showScore ? this.renderScore() : <div></div>}
                {this.state.showEnd ? this.renderEnd() : <div></div>}
                {this.state.showScore && this.state.showEnd ? <div>loading</div> : <div></div>}
            </div>
        )
    }

    render() {
        const defaultOptions = {
            loop: true,
            autoplay: true,
            animationData: data,
            rendererSettings: {
                preserveAspectRatio: "xMidYMid slice"
            }
        };

        return (
            <div>
                {/* <Lottie options={defaultOptions} height={400} width={400} /> */}
                <div className="container">
                    <div className="title">
                        {this.state.isPlaying ? this.renderGame() : this.renderNotPlaying() }
                    </div>
                </div>
            </div>
        );
    }
}
