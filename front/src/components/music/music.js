export default class MusicPlayer {
    constructor(playlist, callback) {
        this.audioHasLoaded = this.audioHasLoaded.bind(this)
        this.preload = this.preload.bind(this)

        this.playlist = playlist
        this.audioPlayer = []
        this.totalAudioLoaded = 0
        this.everyThingIsReadyToPlay = false
        this.loadedCallback = callback
        
        this.preload()
    }   

    audioHasLoaded() {
        this.totalAudioLoaded++

        if (this.totalAudioLoaded == this.playlist.length) {
            this.everyThingIsReadyToPlay = true
            this.loadedCallback()
        }
    }

    play(index) {
        if (this.everyThingIsReadyToPlay) {
            this.pause()
            this.audioPlayer[index].play()
        } else {
            console.log("tous les medias n'ont pas encore sont dans la memoire tampon")
        }
    }

    pause() {
        if (this.everyThingIsReadyToPlay) {
            for(var i = 0; i < this.playlist.length; i++) {
                this.audioPlayer[i].pause()
            } 
        }
    }

    preload() {
        for(var i = 0; i < this.playlist.length; i++) {
            this.audioPlayer[i] = new Audio(this.playlist[i].url)
            this.audioPlayer[i].addEventListener("canplaythrough", this.audioHasLoaded, false)
        }
    }
}