export const PROD = true
export const FRONT_URI = PROD ? "http://music.planchon.io" : "http://localhost:8080"
export const SOCKET_URI = PROD ? "http://music.planchon.io:3000" : "http://localhost:3000"
